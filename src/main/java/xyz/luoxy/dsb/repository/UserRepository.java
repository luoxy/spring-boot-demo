package xyz.luoxy.dsb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xyz.luoxy.dsb.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
     User findByEmail(String email);
}