package xyz.luoxy.dsb.service;

import xyz.luoxy.dsb.model.User;

public interface UserService {
    public User findUserByEmail(String email);

    public User saveUser(User user);
}